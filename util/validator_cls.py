from datetime import datetime
from google.cloud import storage, bigquery
from rich import print
from typing import Union


class Validator:
    def get_gcs_file_last_modified_time(
        self, bucket_name: str, file_prefix: str, project_name: str = None
    ) -> Union[datetime, None]:

        """get the latest last modified time of files with the prefix

        Args:
            bucket_name (str)
            file_prefix (str)
            project_name (str, optional): point to specific project if bucket isn't the default project. Defaults to None.

        Returns:
            Union[str, None]: iso format datetime string in UTC timezone
        """
        gcs = storage.Client(project=project_name)
        blobs = gcs.list_blobs(bucket_or_name=bucket_name, prefix=file_prefix)
        # r = bucket.list_blobs(prefix="2022/03/01", max_results=1)

        try:
            last_created_time = max(blob.time_created.isoformat() for blob in blobs)
            print(
                f'last created files with prefix "{file_prefix}" was {last_created_time}'
            )
            return last_created_time
        except ValueError:
            print(f'there is no file with prefix "{file_prefix}"')
            return None

    def get_bq_table_last_insertion_time(
        self, project_id: str, dataset_id, table_id
    ) -> bigquery.table.RowIterator:
        """_summary_

        Args:
            project_id (str): _description_
            sql_query (str): _description_

        Returns:
            bigquery.table.RowIterator: _description_
        """
        bq = bigquery.Client()
        sql_query = f"""select max(insertion_time) as max_insertion_time
            FROM `{project_id}.{dataset_id}.{table_id}`"""
        job = bq.query(query=sql_query, job_id_prefix="miyagi_monitoring_")
        results = job.result()
        max_insertion_time = next(results)["max_insertion_time"]
        max_insertion_time_in_str = max_insertion_time.isoformat()
        print(
            f'max insertion_time in "{project_id}.{dataset_id}.{table_id}" is {max_insertion_time_in_str}'
        )
        return max_insertion_time_in_str


if __name__ == "__main__":
    # bucket_name = "datadesk-factor-asia-ingestion-dev"
    # file_prefix = "2022/03/03/BOT_"
    v = Validator()
    # time = v.get_gcs_file_last_modified_time(
    #     project_name="cn-ops-spdigital-dev-dev",
    #     bucket_name=bucket_name,
    #     file_prefix=file_prefix,
    # )
    # print(time)
    # print(type(time))
    query = "select max(insertion_time) as max_insertion_time FROM `gbl-imt-ve-datalake-prod.00766_factorasia_hongkong.BOT_DataRaw`"
    output = v.get_bq_table_last_insertion_time(
        project_id="gbl-imt-ve-datalake-prod",
        dataset_id="00766_factorasia_hongkong",
        table_id="BOT_DataRaw",
    )
    print(output)
    pass
# blob = bucket.get_blob("2022/03/01/BOT_DataRaw_ASIA.json")
# https://storage.cloud.google.com/datadesk-factor-asia-ingestion-dev/2022/03/02/BOT_ActivityComment_ASIA.json
# # json_data = blob.download_as_text()
# with open("test.json", "w") as f:
#     blob.download_to_file(f)
