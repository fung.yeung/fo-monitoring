import traceback
from util.validator_cls import Validator
import flask

v = Validator()

def main(request: flask.Request):
    """

    Args:
        request (flask.Request): _description_

    Returns:
        _type_: _description_
    """
    request_json = request.get_json()
    method_name = request_json.get("method_name")
    params = request_json.get("params")

    func = getattr(v, method_name)

    try:
        return func(**params), 200
    except Exception:
        print("there is error")
        err = traceback.format_exc()
        print(err)
        return traceback.format_exc(), 500


if __name__ == "__main__":
    main()

    # get_gcs_file_last_modified_time
    # get_bq_table_last_insertion_time
